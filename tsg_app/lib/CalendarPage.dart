import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CalendarPage extends StatefulWidget {
  @override
  _CalendarPageState createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  CalendarController _controller = CalendarController();
  Map<DateTime, List<dynamic>> _events = {};
  List<dynamic> _selectedEvents = [];
  bool readData = false;
  String _string = "";

  @override
  void initState() {
    super.initState();
  }

  getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('username');
    setState(() {
      _string = username;
    });
    return username;
  }

  @override
  Widget build(BuildContext context) {
    getName();
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Event Tracker'),
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection("eveniments").snapshots(),
        builder: (context, snapshot) {
          return StreamBuilder(
            stream: Firestore.instance.collection("user").snapshots(),
            builder: (context, snapshot2) {
              if (snapshot.hasData && snapshot2.hasData && readData == false) {
                if (snapshot.data.documents.isNotEmpty) {
                  for (var user in snapshot2.data.documents) {
                    if (user['user_name'] == _string && readData == false)
                      snapshot.data.documents.forEach((event) {
                        if ((user['tag_bikeTrips'] == true &&
                                event["eveniment_topic"] == "tag_bikeTrips") ||
                            (user['tag_roadTrips'] == true &&
                                event["eveniment_topic"] == "tag_roadTrips") ||
                            (user['tag_partys'] == true &&
                                event["eveniment_topic"] == "tag_partys") ||
                            (user['tag_concerts'] == true &&
                                event["eveniment_topic"] == "tag_concerts") ||
                            (event["eveniment_topic"] == "tag_" + _string)) {
                          DateTime date = DateTime.parse(
                              event["eveniment_date"] + " 00:00:00");
                          if (_events[date] == null) _events[date] = [];
                          _events[date].add(event);
                        }
                        if (_events.isNotEmpty) readData = true;
                      });
                  }
                } else {
                  _events = {};
                  _selectedEvents = [];
                }
              }
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TableCalendar(
                      events: _events,
                      initialCalendarFormat: CalendarFormat.month,
                      calendarStyle: CalendarStyle(
                          canEventMarkersOverflow: true,
                          todayColor: Colors.orange,
                          selectedColor: Theme.of(context).primaryColor,
                          todayStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                              color: Colors.white)),
                      headerStyle: HeaderStyle(
                        centerHeaderTitle: true,
                        formatButtonDecoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        formatButtonTextStyle: TextStyle(color: Colors.white),
                        formatButtonShowsNext: false,
                      ),
                      startingDayOfWeek: StartingDayOfWeek.monday,
                      onDaySelected: (date, _events, _) {
                        setState(() {
                          _selectedEvents = _events;
                        });
                      },
                      builders: CalendarBuilders(
                        selectedDayBuilder: (context, date, events) =>
                            Container(
                                margin: const EdgeInsets.all(4.0),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.circular(10.0)),
                                child: Text(
                                  date.day.toString(),
                                  style: TextStyle(color: Colors.white),
                                )),
                        todayDayBuilder: (context, date, events) => Container(
                            margin: const EdgeInsets.all(4.0),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(10.0)),
                            child: Text(
                              date.day.toString(),
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                      calendarController: _controller,
                    ),
                    ..._selectedEvents.map((event) => ListTile(
                          title: Text(
                            event["eveniment_date"] +
                                " :" +
                                event["eveniment_title"],
                            style: TextStyle(
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans',
                            ),
                          ),
                        )),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
