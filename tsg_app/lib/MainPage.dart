import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:tsg_app/ProfilePage.dart';
import 'package:tsg_app/CalendarPage.dart';

import 'package:flip_card/flip_card.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String _string = "";
  DateTime dateNow = DateTime.now();

  @override
  void initState() {
    super.initState();
  }

  getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('username');
    setState(() {
      _string = username;
    });
    return username;
  }

  String pickTags(List<String> tags) {
    String tag = "";
    for (var item in tags) tag = tag + item + " ";
    return null;
  }

  Color pickColorForCards(String tag) {
    if (tag == "tag_bikeTrips") return Color(0xFF9400D3);
    if (tag == "tag_roadTrips") return Color(0xFF228B22);
    if (tag == "tag_partys") return Color(0xFFB22222);
    if (tag == "tag_concerts") return Color(0xFFFF8C00);
    if (tag == "tag_" + _string) return Color(0xFFA9A9A9);
    return Color(0xFFFFFFFF);
  }

  Icon pickIconForCards(String tag) {
    if (tag == "tag_bikeTrips")
      return Icon(
        Icons.bike_scooter,
        size: 45.0,
      );
    if (tag == "tag_roadTrips")
      return Icon(
        Icons.directions_walk_outlined,
        size: 45.0,
      );
    if (tag == "tag_partys")
      return Icon(
        Icons.album,
        size: 45.0,
      );
    if (tag == "tag_concerts")
      return Icon(
        Icons.music_note,
        size: 45.0,
      );
    if (tag == "tag_" + _string)
      return Icon(
        Icons.account_circle,
        size: 45.0,
      );
    return Icon(
      Icons.ac_unit,
      size: 45.0,
    );
  }

  Widget _buildListView() {
    return StreamBuilder(
      stream: Firestore.instance.collection("user").snapshots(),
      builder: (context, snapshot) {
        return StreamBuilder(
          stream: Firestore.instance
              .collection("eveniments")
              .orderBy("eveniment_date")
              .snapshots(),
          builder: (context, snapshot2) {
            if (!snapshot.hasData)
              return Text("Loading data... Please Wait...");
            if (!snapshot2.hasData)
              return Text("Loading data... Please Wait...");
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (var user in snapshot.data.documents)
                  if (user['user_name'] == _string)
                    for (var item in snapshot2.data.documents)
                      if (((user['tag_bikeTrips'] == true &&
                                  item["eveniment_topic"] == "tag_bikeTrips") ||
                              (user['tag_roadTrips'] == true &&
                                  item["eveniment_topic"] == "tag_roadTrips") ||
                              (user['tag_partys'] == true &&
                                  item["eveniment_topic"] == "tag_partys") ||
                              (user['tag_concerts'] == true &&
                                  item["eveniment_topic"] == "tag_concerts") ||
                              (item["eveniment_topic"] == "tag_" + _string)) &&
                          DateTime.parse(item["eveniment_date"]).isAfter(
                              DateTime(dateNow.year, dateNow.month,
                                  dateNow.day - 1)))
                        FlipCard(
                          front: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Card(
                                color:
                                    pickColorForCards(item["eveniment_topic"]),
                                elevation: 50,
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                    color: Colors.black,
                                    width: 2.0,
                                  ),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Container(
                                  width: 350,
                                  height: 200,
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        ListTile(
                                          leading: pickIconForCards(
                                              item["eveniment_topic"]),
                                          title: Text(item["eveniment_title"],
                                              style: TextStyle(
                                                color: Color(0xFFFFFFFF),
                                                letterSpacing: 1.5,
                                                fontSize: 24.0,
                                                fontWeight: FontWeight.bold,
                                                fontFamily: 'OpenSans',
                                              ),
                                              textAlign: TextAlign.center),
                                          subtitle: Text(
                                            item["eveniment_date"],
                                            style: TextStyle(
                                              color: Color(0xFFFFFFFF),
                                              letterSpacing: 1.5,
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: 'OpenSans',
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ]),
                                ),
                              )),
                          back: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Card(
                              color: pickColorForCards(item["eveniment_topic"]),
                              shape: RoundedRectangleBorder(
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 2.0,
                                ),
                                borderRadius: BorderRadius.circular(30),
                              ),
                              elevation: 50,
                              child: Container(
                                width: 350,
                                height: 200,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "About: " + item["eveniment_description"],
                                      style: TextStyle(
                                        color: Color(0xFFFFFFFF),
                                        letterSpacing: 1.5,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'OpenSans',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      "Where: " + item["eveniment_location"],
                                      style: TextStyle(
                                        color: Color(0xFFFFFFFF),
                                        letterSpacing: 1.5,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'OpenSans',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Text(
                                      "Tags: " +
                                          item["eveniment_tags"]
                                              .toString()
                                              .substring(
                                                  1,
                                                  item["eveniment_tags"]
                                                          .toString()
                                                          .length -
                                                      1),
                                      style: TextStyle(
                                        color: Color(0xFFFFFFFF),
                                        letterSpacing: 1.5,
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'OpenSans',
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
              ],
            );
          },
        );
      },
    );
  }

  Widget _profileButton() {
    return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ProfilePage()));
        },
        child: Icon(
          Icons.account_circle,
          size: 26.0,
        ));
  }

  Widget _calendarButton() {
    return GestureDetector(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => CalendarPage()));
        },
        child: Icon(
          Icons.calendar_today,
          size: 26.0,
        ));
  }

  @override
  Widget build(BuildContext context) {
    getName();
    return Scaffold(
      appBar: AppBar(
        title: Text("Your Event Tracker"),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0), child: _calendarButton()),
          Padding(
              padding: EdgeInsets.only(right: 20.0), child: _profileButton()),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF73AEF5),
                  Color(0xFF61A4F1),
                  Color(0xFF478De0),
                  Color(0xFF398Ae5),
                ],
                stops: [0.1, 0.4, 0.7, 0.9],
              ),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 20.0,
                vertical: 40.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildListView(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
