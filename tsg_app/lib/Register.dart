import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'package:toast/toast.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

class Register extends StatefulWidget {
  Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String username = "";
  String password = "";
  String email = "";
  bool _checked1 = false;
  bool _checked2 = false;
  bool _checked3 = false;
  bool _checked4 = false;

  void controllerSet(TextEditingController controller) {
    controller.addListener(() {
      final text = controller.text;
      controller.value = controller.value.copyWith(
        text: text,
        selection:
            TextSelection(baseOffset: text.length, extentOffset: text.length),
        composing: TextRange.empty,
      );
    });
  }

  @override
  void initState() {
    controllerSet(usernameController);
    controllerSet(passwordController);
    controllerSet(firstNameController);
    controllerSet(lastNameController);
    controllerSet(emailController);
    controllerSet(phoneController);
    super.initState();
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  bool validation(AsyncSnapshot<dynamic> snapshot) {
    bool ok = true;
    if (usernameController.text == "") {
      ok = false;
      Toast.show("Please, fill the username textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    } else {
      for (var user in snapshot.data.documents) {
        if (usernameController.text == user["user_name"]) {
          ok = false;
          Toast.show("Username already taken", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        }
      }
    }

    if (passwordController.text == "") {
      ok = false;
      Toast.show("Please, fill the password textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
    if (emailController.text == "") {
      ok = false;
      Toast.show("Please, fill the email textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
    return ok;
  }

  showPostSnackBar() {
    final snackBar = new SnackBar(
      content: Text(
        "Your account has been created",
        style: TextStyle(
            fontFamily: 'OpenSans', fontWeight: FontWeight.bold, fontSize: 20),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.green,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final lastNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final phoneController = TextEditingController();
  final emailController = TextEditingController();
  final adminController = TextEditingController();

  Widget _buildSubscriptionsText() {
    return Text(
      "Choose your Subscriptions",
      style: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 22.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _tagBikeTripsCheckBox() {
    return Card(
        color: Colors.white,
        elevation: 50,
        child: Container(
            width: 400,
            height: 50,
            child: CheckboxListTile(
                title: Text("Bike Trips.",
                    style: TextStyle(
                        letterSpacing: 1.5,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans')),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checked1,
                onChanged: (bool value) {
                  setState(() {
                    _checked1 = value;
                  });
                })));
  }

  Widget _tagRoadTripsCheckBox() {
    return Card(
        color: Colors.white,
        elevation: 50,
        child: Container(
            width: 400,
            height: 50,
            child: CheckboxListTile(
                title: Text("Road Trips.",
                    style: TextStyle(
                        letterSpacing: 1.5,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans')),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checked2,
                onChanged: (bool value) {
                  setState(() {
                    _checked2 = value;
                  });
                })));
  }

  Widget _tagPartysCheckBox() {
    return Card(
        color: Colors.white,
        elevation: 50,
        child: Container(
            width: 400,
            height: 50,
            child: CheckboxListTile(
                title: Text("Partys.",
                    style: TextStyle(
                        letterSpacing: 1.5,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans')),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checked3,
                onChanged: (bool value) {
                  setState(() {
                    _checked3 = value;
                  });
                })));
  }

  Widget _tagConcertsCheckBox() {
    return Card(
        color: Colors.white,
        elevation: 50,
        child: Container(
            width: 400,
            height: 50,
            child: CheckboxListTile(
                title: Text("Concerts.",
                    style: TextStyle(
                        letterSpacing: 1.5,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'OpenSans')),
                controlAffinity: ListTileControlAffinity.platform,
                value: _checked4,
                onChanged: (bool value) {
                  setState(() {
                    _checked4 = value;
                  });
                })));
  }

  Widget _buildUsernameText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Username',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: usernameController..text = username,
            onChanged: (value) {
              username = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter your username',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Password',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: passwordController..text = password,
            onChanged: (value) {
              password = value;
            },
            obscureText: true,
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '  Enter your password',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildEmailText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Email',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: emailController..text = email,
            onChanged: (value) {
              email = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter your email',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildRegisterBtn() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          return Container(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            width: double.infinity,
            child: RaisedButton(
              elevation: 5.0,
              onPressed: () {
                if (validation(snapshot)) {
                  if (usernameController.text
                      .contains(new RegExp(r'([a-z]||[A-Z])\w+.admin'))) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                            title: TextField(
                              controller: adminController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                contentPadding: EdgeInsets.only(top: 14.0),
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                ),
                                hintText: 'Enter the admin code',
                                hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontFamily: 'OpenSans',
                                ),
                              ),
                            ),
                            content: IconButton(
                                icon: Icon(Icons.check),
                                onPressed: () {
                                  if (adminController.text == "12345") {
                                    Map<String, dynamic> user = {
                                      "user_name": usernameController.text,
                                      "user_password": passwordController.text,
                                      "user_email": emailController.text,
                                      "tag_bikeTrips": _checked1,
                                      "tag_roadTrips": _checked2,
                                      "tag_partys": _checked3,
                                      "tag_concerts": _checked4,
                                    };

                                    Firestore.instance
                                        .collection("user")
                                        .add(user)
                                        .catchError((e) {
                                      print(e);
                                    });
                                    showPostSnackBar();
                                    Navigator.of(context, rootNavigator: true)
                                        .pop('dialog');
                                  } else {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          content: Text('Wrong code'),
                                        );
                                      },
                                    );
                                  }
                                }));
                      },
                    );
                  } else {
                    Map<String, dynamic> user = {
                      "user_name": usernameController.text,
                      "user_password": passwordController.text,
                      "user_email": emailController.text,
                      "tag_bikeTrips": _checked1,
                      "tag_roadTrips": _checked2,
                      "tag_partys": _checked3,
                      "tag_concerts": _checked4,
                    };

                    Firestore.instance
                        .collection("user")
                        .add(user)
                        .catchError((e) {
                      print(e);
                    });
                    showPostSnackBar();
                  }
                }
              },
              padding: EdgeInsets.all(15.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.white,
              child: Text(
                'Register',
                style: TextStyle(
                  color: Color(0xFF527DAA),
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color(0xFF73AEF5),
                      Color(0xFF61A4F1),
                      Color(0xFF478DE0),
                      Color(0xFF398AE5),
                    ],
                    stops: [0.1, 0.4, 0.7, 0.9],
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 40.0,
                    vertical: 80.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Register your account!",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'OpenSans',
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 30.0),
                      SizedBox(
                        height: 50.0,
                      ),
                      _buildUsernameText(),
                      _buildPasswordText(),
                      _buildEmailText(),
                      SizedBox(height: 30.0),
                      _buildSubscriptionsText(),
                      SizedBox(height: 30.0),
                      _tagBikeTripsCheckBox(),
                      _tagRoadTripsCheckBox(),
                      _tagPartysCheckBox(),
                      _tagConcertsCheckBox(),
                      _buildRegisterBtn()
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
