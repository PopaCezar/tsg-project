import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:toast/toast.dart';

class AdministratorPage extends StatefulWidget {
  AdministratorPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AdministratorPageState createState() => _AdministratorPageState();
}

class _AdministratorPageState extends State<AdministratorPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _title = "";
  String _description = "";
  String _location = "";
  String _tagsFullString = "";
  DateTime _selectedDate = DateTime.now();
  int group = 1;
  String _tag = "tag_bikeTrips";

  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final locationController = TextEditingController();
  final tagsController = TextEditingController();

  void controllerSet(TextEditingController controller) {
    controller.addListener(() {
      final text = controller.text;
      controller.value = controller.value.copyWith(
        text: text,
        selection:
            TextSelection(baseOffset: text.length, extentOffset: text.length),
        composing: TextRange.empty,
      );
    });
  }

  @override
  void initState() {
    controllerSet(titleController);
    controllerSet(descriptionController);
    controllerSet(locationController);
    controllerSet(tagsController);
    super.initState();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime.now(),
      lastDate: DateTime(2022, 1),
    );
    if (picked != null && picked != _selectedDate)
      setState(() {
        _selectedDate = picked;
      });
  }

  showPostSnackBar() {
    final snackBar = new SnackBar(
      content: Text(
        "Event Added.",
        style: TextStyle(
            fontFamily: 'OpenSans', fontWeight: FontWeight.bold, fontSize: 20),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.green,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  bool validation(AsyncSnapshot<dynamic> snapshot) {
    bool ok = true;
    if (titleController.text == "") {
      ok = false;
      Toast.show("Please, fill the title textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

    if (descriptionController.text == "") {
      ok = false;
      Toast.show("Please, fill the description textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
    if (locationController.text == "") {
      ok = false;
      Toast.show("Please, fill the location textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

    if (tagsController.text == "") {
      ok = false;
      Toast.show("Please, fill the tags textfield", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }

    RegExp exp = new RegExp(r"((#[a-z])\w*,)+");
    Iterable<Match> matches = exp.allMatches(tagsController.text);
    if (matches.isEmpty) {
      ok = false;
      Toast.show("Tags are not corect", context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    }
    return ok;
  }

  Widget _buildSubscriptionsText() {
    return Text(
      "Choose a topic",
      style: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 22.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _buildTagRadios() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            Text(
              ' Bike Trip',
            ),
            Radio(
                value: 1,
                groupValue: group,
                activeColor: Colors.red,
                onChanged: (T) {
                  _tag = "tag_bikeTrips";
                  setState(() {
                    group = T;
                  });
                }),
            Text(
              ' Road Trip',
            ),
            Radio(
                value: 2,
                groupValue: group,
                activeColor: Colors.red,
                onChanged: (T) {
                  _tag = "tag_roadTrips";
                  setState(() {
                    group = T;
                  });
                }),
          ],
        ),
        Row(
          children: [
            Text(
              ' Party      ',
            ),
            Radio(
                value: 3,
                groupValue: group,
                activeColor: Colors.red,
                onChanged: (T) {
                  _tag = "tag_Partys";
                  setState(() {
                    group = T;
                  });
                }),
            Text(
              ' Concert   ',
            ),
            Radio(
                value: 4,
                groupValue: group,
                activeColor: Colors.red,
                onChanged: (T) {
                  _tag = "tag_Concerts";
                  setState(() {
                    group = T;
                  });
                })
          ],
        ),
      ],
    );
  }

  Widget _buildTitle() {
    return Text(
      "Add a public Event",
      style: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 36.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _buildSelectDateButton() {
    return Column(
      children: <Widget>[
        RaisedButton(
          onPressed: () => _selectDate(context),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.white,
          child: ListTile(
            leading: Icon(Icons.calendar_today),
            title: Text("        Date",
                style: TextStyle(
                  color: Color(0xFF527DAA),
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                ),
                textAlign: TextAlign.left),
          ),
        ),
        SizedBox(
          height: 20.0,
        ),
        Text(
          "${_selectedDate.toLocal()}".split(' ')[0],
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ],
    );
  }

  Widget _buildTitleText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Title',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: titleController..text = _title,
            onChanged: (value) {
              _title = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter the title of the eveniment',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDescriptionText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Description',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: descriptionController..text = _description,
            onChanged: (value) {
              _description = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter the description of the eveniment',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLocationText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Location',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: locationController..text = _location,
            onChanged: (value) {
              _location = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter the location of the eveniment',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTagsText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          ' Tags(Write like :#tag,)',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Color(0xFF6CA8F1),
            borderRadius: BorderRadius.circular(10.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6.0,
                offset: Offset(0, 2),
              ),
            ],
          ),
          height: 60.0,
          child: TextField(
            controller: tagsController..text = _tagsFullString,
            onChanged: (value) {
              _tagsFullString = value;
            },
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              hintText: '   Enter the tags of the eveniment',
              hintStyle: TextStyle(
                color: Colors.white54,
                fontFamily: 'OpenSans',
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildAddEvenimentBtn() {
    return StreamBuilder(
        stream: Firestore.instance.collection("eveniment").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          return Container(
            padding: EdgeInsets.symmetric(vertical: 15.0),
            width: double.infinity,
            child: RaisedButton(
              elevation: 5.0,
              onPressed: () {
                if (validation(snapshot)) {
                  var strings = tagsController.text.split(",");
                  Map<String, dynamic> eveniment = {
                    "eveniment_title": titleController.text,
                    "eveniment_description": descriptionController.text,
                    "eveniment_location": locationController.text,
                    "eveniment_topic": _tag,
                    "eveniment_date": DateFormat('yyyy-MM-dd')
                        .format(_selectedDate)
                        .toString(),
                    "eveniment_tags": strings.sublist(0, strings.length - 1)
                  };

                  Firestore.instance
                      .collection("eveniments")
                      .add(eveniment)
                      .catchError((e) {
                    print(e);
                  });
                  showPostSnackBar();
                }
              },
              padding: EdgeInsets.all(15.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.white,
              child: Text(
                'Add event',
                style: TextStyle(
                  color: Color(0xFF527DAA),
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF73AEF5),
                  Color(0xFF61A4F1),
                  Color(0xFF478De0),
                  Color(0xFF398Ae5),
                ],
                stops: [0.1, 0.4, 0.7, 0.9],
              ),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 60.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildTitle(),
                  SizedBox(height: 30.0),
                  _buildTitleText(),
                  _buildDescriptionText(),
                  _buildLocationText(),
                  _buildSubscriptionsText(),
                  _buildTagRadios(),
                  _buildTagsText(),
                  SizedBox(height: 30.0),
                  _buildSelectDateButton(),
                  _buildAddEvenimentBtn()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
