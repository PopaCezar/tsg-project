import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:tsg_app/UserEvenimentPage.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String _string = "";
  bool checked1 = false;
  bool checked2 = false;
  bool checked3 = false;
  bool checked4 = false;

  bool ok1 = false;
  bool ok2 = false;
  bool ok3 = false;
  bool ok4 = false;

  @override
  void initState() {
    super.initState();
  }

  showPostSnackBar() {
    final snackBar = new SnackBar(
      content: Text(
        "Saved.",
        style: TextStyle(
            fontFamily: 'OpenSans', fontWeight: FontWeight.bold, fontSize: 20),
        textAlign: TextAlign.center,
      ),
      backgroundColor: Colors.green,
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  getName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('username');
    setState(() {
      _string = username;
    });
    return username;
  }

  Widget _buildMainText() {
    return Text(
      "Welcome to your Profile",
      style: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 36.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _buildSubscriptionsText() {
    return Text(
      "Manage your Subscriptions",
      style: TextStyle(
        color: Color(0xFFFFFFFF),
        letterSpacing: 1.5,
        fontSize: 22.0,
        fontWeight: FontWeight.bold,
        fontFamily: 'OpenSans',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _buildInfoTexts() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          for (var user in snapshot.data.documents) {
            if (user["user_name"].toString() == _string) {
              return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Username: " + user["user_name"],
                      style: TextStyle(
                          letterSpacing: 1.5,
                          color: Colors.white,
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OpenSans'),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      "Email: " + user["user_email"],
                      style: TextStyle(
                          letterSpacing: 1.5,
                          fontSize: 22.0,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'OpenSans'),
                      textAlign: TextAlign.left,
                    ),
                  ]);
            }
          }
        });
  }

  Widget _tagBikeTripsCheckBox() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          for (var user in snapshot.data.documents) {
            if (user["user_name"].toString() == _string && ok1 == false) {
              checked1 = user["tag_bikeTrips"];
              ok1 = true;
            }
          }
          return Card(
              color: Colors.white,
              elevation: 50,
              child: Container(
                  width: 400,
                  height: 50,
                  child: CheckboxListTile(
                      title: Text("Bike Trips.",
                          style: TextStyle(
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans')),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: checked1,
                      onChanged: (bool value) {
                        setState(() {
                          checked1 = value;
                        });
                      })));
        });
  }

  Widget _tagRoadTripsCheckBox() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          for (var user in snapshot.data.documents) {
            if (user["user_name"].toString() == _string && ok2 == false) {
              checked2 = user["tag_roadTrips"];
              ok2 = true;
            }
          }
          return Card(
              color: Colors.white,
              elevation: 50,
              child: Container(
                  width: 400,
                  height: 50,
                  child: CheckboxListTile(
                      title: Text("Road Trips.",
                          style: TextStyle(
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans')),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: checked2,
                      onChanged: (bool value) {
                        setState(() {
                          checked2 = value;
                        });
                      })));
        });
  }

  Widget _tagPartysCheckBox() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          for (var user in snapshot.data.documents) {
            if (user["user_name"].toString() == _string && ok3 == false) {
              checked3 = user["tag_partys"];
              ok3 = true;
            }
          }
          return Card(
              color: Colors.white,
              elevation: 50,
              child: Container(
                  width: 400,
                  height: 50,
                  child: CheckboxListTile(
                      title: Text("Partys.",
                          style: TextStyle(
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans')),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: checked3,
                      onChanged: (bool value) {
                        setState(() {
                          checked3 = value;
                        });
                      })));
        });
  }

  Widget _tagConcertsCheckBox() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          for (var user in snapshot.data.documents) {
            if (user["user_name"].toString() == _string && ok4 == false) {
              checked4 = user["tag_concerts"];
              ok4 = true;
            }
          }
          return Card(
              color: Colors.white,
              elevation: 50,
              child: Container(
                  width: 400,
                  height: 50,
                  child: CheckboxListTile(
                      title: Text("Concerts.",
                          style: TextStyle(
                              letterSpacing: 1.5,
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'OpenSans')),
                      controlAffinity: ListTileControlAffinity.platform,
                      value: checked4,
                      onChanged: (bool value) {
                        setState(() {
                          checked4 = value;
                        });
                      })));
        });
  }

  Widget _buildUpdateButton() {
    return StreamBuilder(
        stream: Firestore.instance.collection("user").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Text("Loading data... Please Wait...");
          return Container(
            padding: EdgeInsets.symmetric(vertical: 25.0),
            width: double.infinity,
            child: RaisedButton(
              elevation: 5.0,
              onPressed: () {
                for (var user in snapshot.data.documents) {
                  if (user["user_name"].toString() == _string) {
                    Firestore.instance
                        .collection('user')
                        .document(user.documentID)
                        .updateData({
                      'tag_bikeTrips': checked1,
                      'tag_roadTrips': checked2,
                      'tag_partys': checked3,
                      'tag_concerts': checked4
                    });
                    showPostSnackBar();
                  }
                }
              },
              padding: EdgeInsets.all(15.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              color: Colors.white,
              child: Text(
                'Save',
                style: TextStyle(
                  color: Color(0xFF527DAA),
                  letterSpacing: 1.5,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          );
        });
  }

  Widget _buildAddEvenimentBtn() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 25.0),
        width: double.infinity,
        child: RaisedButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => UserEvenimentPage()));
          },
          padding: EdgeInsets.all(15.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          color: Colors.white,
          child: Text(
            'Add your Event',
            style: TextStyle(
              color: Color(0xFF527DAA),
              letterSpacing: 1.5,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    getName();
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF73AEF5),
                  Color(0xFF61A4F1),
                  Color(0xFF478De0),
                  Color(0xFF398Ae5),
                ],
                stops: [0.1, 0.4, 0.7, 0.9],
              ),
            ),
          ),
          Container(
            height: double.infinity,
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 40.0,
                vertical: 60.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _buildMainText(),
                  SizedBox(height: 30.0),
                  _buildInfoTexts(),
                  SizedBox(height: 30.0),
                  _buildSubscriptionsText(),
                  _tagBikeTripsCheckBox(),
                  _tagRoadTripsCheckBox(),
                  _tagPartysCheckBox(),
                  _tagConcertsCheckBox(),
                  _buildUpdateButton(),
                  _buildAddEvenimentBtn()
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
